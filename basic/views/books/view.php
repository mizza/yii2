<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Books */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Books'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="books-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p class="text-center"><?=Html::img($model->preview, ['height'=>300, 'alt'=>$this->title]);?></p>
     <blockquote>
      <p><?=$model->author->formatedName?>, <?=Yii::$app->formatter->asDatetime($model->date, "php:d.m.Y")?></p>
    </blockquote>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'date_create',
            'date_update',
        ],
    ]) ?>

</div>
