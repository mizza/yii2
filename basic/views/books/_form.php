<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Authors;
use \yii\jui\yii\base\Model;

/* @var $this yii\web\View */
/* @var $model app\models\Books */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="books-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'preview')->fileInput() ?>

    <?= $form->field($model, 'date')->textInput(['class'=>'datepicker form-control']) ?>
    <?// ->widget(\yii\jui\DatePicker::classname(), [
    //     //'language' => 'ru',
    //     //'dateFormat' => 'yyyy-MM-dd',
    // ]) ?>

    <?=  $form->field($model, 'author_id')
        ->dropDownList(
            Authors::getFormatedList(),
            ['prompt'=>'']    // options
        );?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Добавить') : Yii::t('app', 'Изменить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
