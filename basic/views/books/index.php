<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BooksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Книги');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="books-index">
<?php //phpinfo() ?>
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= $this->render('_search', [
        'model' => $searchModel,
    ]) ?>

    <p>
        <?= Html::a(Yii::t('app', 'Добавить книгу'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'filterPosition' => "",
        'columns' => [
            [
                'attribute' => 'id',
                'contentOptions' => ['width'=>30] // ужимаем колонку
            ],
            'name',
            [
                'attribute' => 'preview',
                'format' => 'html',
                'value' => function($model, $key, $index, $column){
                    return \yii\helpers\Html::a(\yii\helpers\Html::img($model->preview, ["width"=>100]), $model->preview, ["class"=>"fancybox"]);
                }, 
                'contentOptions' => ['width' => 100]
            ],
            [
                'attribute' => 'author_id',
                'value' => 'author.formatedName'
            ],
            // 'author.formatedName',
            [
                'attribute' => 'date',
                'value' => function($model, $key, $index, $column){
                    return Yii::$app->formatter->asDatetime($model->date, "php:d.m.Y");
                }, 
            ],
            [
                'attribute' => 'date_create',
                'value' => function($model, $key, $index, $column){
                    return Yii::$app->formatter->asDatetime($model->date_create, "php:d.m.Y H:i:s");
                }, 
            ],  
            // 'date_update',

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Кнопки действий',
                'buttons'=>[
                    'view'=>
                        function ($url, $model) {
                            $customurl=Yii::$app->getUrlManager()->createUrl(['books/view','id'=>$model->id, 'mode'=>'ajax']); //$model->id для AR
                            return \yii\helpers\Html::a( 
                                '<span class="glyphicon glyphicon-eye-open"></span>', 
                                $customurl,
                                [
                                    'title' => Yii::t('yii', 'View'), 
                                    'onclick'=>"
                                         $.ajax({
                                            type     :'GET',
                                            cache    : false,
                                            url  :  $(this).attr('href'),
                                            success  : function(response) {
                                                $('#viewModal').find('.modal-body').html(response).end().modal('show');
                                            }
                                        });
                                    return false;",
                                ]);
                        }
                ],
                'contentOptions' => ['align'=>'center']
            ],
        ],
    ]); ?>

</div>

<?
Modal::begin([
    'header' => '<h2>Просмотр книги</h2>',
    'id' => 'viewModal',
]);
Modal::end();
?>

