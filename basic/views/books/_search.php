<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
// use yii\jui\DatePicker;
use app\models\Authors;

/* @var $this yii\web\View */
/* @var $model app\models\BooksSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="books-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">
        <div class="col-xs-4">
            <?
                echo $form->field($model, 'author_id')
                ->dropDownList(
                    Authors::getFormatedList(),
                    ['prompt'=>'']    // options
                );
            ?>
        </div>
        <div class="col-xs-4">
            <?= $form->field($model, 'name') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-8">
            Дата выхода книги: <?= $form->field($model, 'dateFrom', ['template' => "{input}"])->textInput(['class'=>'datepicker form-control'])->label("")?>
            по <?= $form->field($model, 'dateTo', ['template' => "{input}"])->textInput(['class'=>'datepicker form-control'])->label("")?>

            <?// ->widget(DatePicker::classname(), [
            //     'language' => 'ru',
            //     'dateFormat' => 'yyyy-MM-dd',
            // ]) ?>
        </div>
    </div>

    <?php // echo $form->field($model, 'date') ?>

    <?php // echo $form->field($model, 'author_id') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Искать'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
