<?php

namespace app\models;

use Yii;
use \yii\db\CDbExpression;

/**
 * This is the model class for table "books".
 *
 * @property integer $id
 * @property string $name
 * @property string $date_create
 * @property string $date_update
 * @property string $preview
 * @property string $date
 * @property integer $author_id
 *
 * @property Authors $author
 */
class Books extends \yii\db\ActiveRecord
{
    private $oldPreview;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'books';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'date_create', 'date_update', 'author_id'], 'required'],
            [['preview'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, gif'],
            [['date_create', 'date_update', 'date'], 'safe'],
            [['author_id'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['save'] = ['name','date_create', 'date_update', 'preview', 'date', 'author_id'];//Scenario Values Only Accepted
        return $scenarios;
    }

    public function upload()
    {
        $path = 'uploads/' . uniqid() . '.' . $this->preview->extension;
        $uploaded = $this->preview->saveAs($path);
        if ($uploaded){
            $this->preview = $path;
            return true;
        }else{
            return false;
        }
        
    }

    public function afterFind(){
        $this->oldPreview = $this->preview;
    }

    public function beforeValidate(){
        if ($this->scenario == "save"){
            if ($this->isNewRecord)
                $this->date_create = date("Y-m-d H:i:s");
            $this->date_update = date("Y-m-d H:i:s");
        }
        $this->date = date("Y-m-d", strtotime($this->date));
        return parent::beforeValidate();
        
    }

    public function beforeSave($insert){        
        if (parent::beforeSave($insert)){
            if (!empty($this->preview)){
                if ($this->upload()){
                    return true;
                }else{
                    $this->errors['preview'] = "Не удаётся сохранить файл";
                    return false;
                }
            }else{
                $this->preview = $this->oldPreview;
                return true;
            }

        }
        
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Название'),
            'date_create' => Yii::t('app', 'Дата создания'),
            'date_update' => Yii::t('app', 'Дата изменения'),
            'tpreview' => Yii::t('app', 'Картинка'),
            'preview' => Yii::t('app', 'Картинка'),
            'date' => Yii::t('app', 'Дата выхода книги'),
            'author_id' => Yii::t('app', 'Автор'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Authors::className(), ['id' => 'author_id']);
    }

}
