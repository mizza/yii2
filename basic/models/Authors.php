<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "authors".
 *
 * @property integer $id
 * @property string $firstname
 * @property string $lastname
 *
 * @property Books[] $books
 */
class Authors extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'authors';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstname', 'lastname'], 'required'],
            [['firstname', 'lastname'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'firstname' => Yii::t('app', 'Имя'),
            'lastname' => Yii::t('app', 'Фамилия'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBooks()
    {
        return $this->hasMany(Books::className(), ['author_id' => 'id']);
    }

    /**
    * Возвращает массив вида [id => 'И.Фамилия', ...]
    * Используется для выпадающих списков в формах
    * Можно было бы обойтись ArrayHelper::map(Authors::find()->all(), 'id', 'lastname'), но я решил сделать так, чтобы иметь более широкий доступ к форматированию записей
    * @return array
    */
    public static function getFormatedList(){
        $result = [];
        $aAuthors = Authors::find()->all();
        foreach ($aAuthors as $key => $oAuthor) {
            $name = $oAuthor->firstname ? mb_substr($oAuthor->firstname, 0,1).". " : "";
            $surname = $oAuthor->lastname;
            $result[$oAuthor->id] = $name.$surname;
        }
        return $result;
    }

    /**
    * Возвращает отформатированное имя
    * @return string
    */
    public function getFormatedName(){
        
        
        $name = $this->firstname ? mb_substr($this->firstname, 0,1).". " : "";
        $surname = $this->lastname;
        return $name.$surname;
        
        
    }
}
