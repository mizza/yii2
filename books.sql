-- phpMyAdmin SQL Dump
-- version 4.0.10.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1:3306
-- Generation Time: Aug 07, 2015 at 04:49 PM
-- Server version: 5.5.41-log
-- PHP Version: 5.4.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `books`
--

-- --------------------------------------------------------

--
-- Table structure for table `authors`
--

CREATE TABLE IF NOT EXISTS `authors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(100) NOT NULL COMMENT 'Имя',
  `lastname` varchar(100) NOT NULL COMMENT 'Фамилия',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Авторы' AUTO_INCREMENT=7 ;

--
-- Dumping data for table `authors`
--

INSERT INTO `authors` (`id`, `firstname`, `lastname`) VALUES
(1, 'Лев', 'Толстой'),
(2, 'Михаил', 'Булгаков'),
(3, 'Александр', 'Солженицын'),
(4, 'Рей', 'Бредбери'),
(5, 'Эрих Мария', 'Ремарк'),
(6, 'Евгений', 'Замятин');

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE IF NOT EXISTS `books` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT 'Название',
  `date_create` datetime NOT NULL COMMENT 'Дата создания',
  `date_update` datetime NOT NULL COMMENT 'Дата изменения',
  `preview` varchar(255) DEFAULT NULL COMMENT 'Картинка',
  `date` date DEFAULT NULL COMMENT 'Дата выхода книги',
  `author_id` int(11) NOT NULL COMMENT 'Автор',
  PRIMARY KEY (`id`),
  KEY `author_id` (`author_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Книги' AUTO_INCREMENT=10 ;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `name`, `date_create`, `date_update`, `preview`, `date`, `author_id`) VALUES
(3, 'Война и мир', '2015-08-06 22:39:04', '2015-08-06 22:39:04', 'uploads/55c3b7d8334f8.gif', '1970-01-01', 1),
(4, 'Анна Каренина', '2015-08-06 22:41:54', '2015-08-06 22:41:54', 'uploads/55c3b882128b2.jpg', '1970-01-01', 1),
(5, 'Мастер и Маргарита', '2015-08-06 22:43:02', '2015-08-06 22:43:02', 'uploads/55c3b8c6e5e8d.jpg', '1966-09-01', 2),
(6, 'Собачье сердце', '2015-08-06 22:43:42', '2015-08-06 22:43:42', 'uploads/55c3b8ee50b5c.jpg', '1987-07-08', 2),
(8, 'Один день Ивана Денисовича', '2015-08-06 22:49:53', '2015-08-06 22:49:53', 'uploads/55c3ba6189225.jpg', '1962-08-05', 3),
(9, 'Красное колесо', '2015-08-06 22:50:48', '2015-08-06 22:50:48', 'uploads/55c3ba981d65b.jpg', '1991-04-06', 3);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `books`
--
ALTER TABLE `books`
  ADD CONSTRAINT `books_ibfk_1` FOREIGN KEY (`author_id`) REFERENCES `authors` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
